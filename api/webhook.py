import json
from telegram import Bot
from telegram.ext import CommandHandler, MessageHandler, Filters, Dispatcher

def webhook(request):
    try:
        data = json.loads(request.body.decode('UTF-8'))
        bot = Bot(token="YOUR_BOT_TOKEN")
        
        # Do something with the update here, like handling a message
        # Example:
        update = Update.de_json(data, bot)
        dispatcher = Dispatcher(bot, None, workers=0)
        dispatcher.process_update(update)

        return "OK", 200
    except Exception as e:
        return str(e), 500
